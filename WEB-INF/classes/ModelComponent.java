import java.sql.*;
import java.util.*;
public class ModelComponent
{
	public static int insertEmployee(int eno,String ename,double esal,String eaddr)
	{
		int x=0;
		try
		{
			Class.forName("oracle.jdbc.OracleDriver");
			Connection con=DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:XE","scott","tiger");
			PreparedStatement pst=con.prepareStatement("insert into employees1 values(?,?,?,?)");
			pst.setInt(1,eno);
			pst.setString(2,ename);
			pst.setDouble(3,esal);
			pst.setString(4,eaddr);
			x= pst.executeUpdate();
			con.close();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return x;
	}
	public static List<Employee> getAllEmployeesInfo()
	{
		ArrayList<Employee> list= new ArrayList<Employee>();
		try
		{
			Class.forName("oracle.jdbc.OracleDriver");
			Connection con=DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:XE","scott","tiger");
			Statement st=con.createStatement();
			ResultSet rs=st.executeQuery("select * from employees1");
			while(rs.next())
			{
				int eno=rs.getInt(1);
				String ename=rs.getString(2);
				double esal=rs.getDouble(3);
				String eaddr=rs.getString(4);
				Employee e = new Employee();
				e.setEno(eno);
				e.setEname(ename);
				e.setEsal(esal);
				e.setEaddr(eaddr);
				list.add(e);
			}
			con.close();

		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return list;
	}
}
